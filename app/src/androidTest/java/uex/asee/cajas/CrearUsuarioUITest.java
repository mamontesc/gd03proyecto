package uex.asee.cajas;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import uex.asee.cajas.Actividades.Login;

@RunWith(AndroidJUnit4.class)
public class CrearUsuarioUITest {

    @Rule
    public ActivityScenarioRule<Login> mActivityRule =
            new ActivityScenarioRule<>(Login.class);

    //Debido a que la aplicación no permite eliminar usuarios desde la aplicación (sólo desde la API)
    //si se quiere realizar el test varias veces bastará´con cambiar los Strings que aparecen dentro
    //del test
    @Test
    public void shouldCreateUser() {
        String testingString = "UsuarioPrueba";
        String testEmail = "usuarioPrueba@gmail.com";
        //Se accede a la página de registro de usuario
        onView(withId(R.id.nRegButton)).perform(click());
        //Se introducen los valores para crear un nuevo usuario
        onView(withId(R.id.emailreg)).perform(typeText(testEmail),closeSoftKeyboard());
        onView(withId(R.id.passwordreg)).perform(typeText(testingString),closeSoftKeyboard());
        onView(withId(R.id.passwordreg2)).perform(typeText(testingString),closeSoftKeyboard());
        onView(withId(R.id.usernamereg)).perform(typeText(testingString),closeSoftKeyboard());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Se pulsa el botón de registro de usuario
        onView(withId(R.id.registerButton)).perform(click());
    }
}
