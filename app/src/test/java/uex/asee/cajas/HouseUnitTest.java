package uex.asee.cajas;

import static org.junit.Assert.assertEquals;

import uex.asee.cajas.Models.House;

import org.junit.Test;

public class HouseUnitTest {

    @Test
    public void shouldGetIdOfHouse() {
        House house = new House();
        house.set_id("1");
        assertEquals("1",house.get_id());
    }

    @Test
    public void shouldGetNameOfHouse() {
        House house = new House();
        house.setName("house");
        assertEquals("house",house.getName());
    }

    @Test
    public void shouldGetDescriptionOfHouse() {
        House house = new House();
        house.setDescription("desc");
        assertEquals("desc",house.getDescription());
    }
}
