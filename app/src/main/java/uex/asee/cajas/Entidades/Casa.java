package uex.asee.cajas.Entidades;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "casa")
public class Casa {
    @Ignore
    public static final String ITEM_SEP = System.getProperty("line.separator");

    @Ignore
    public static final String ID = "ID";

    @Ignore
    public static final String NAME = "NAME";

    @Ignore
    public static final String DESCRIPTION = "DESCRIPTION";

    @PrimaryKey
    @NonNull
    private String id;

    @ColumnInfo(name = "name")
    private String name = new String();

    @ColumnInfo(name = "description")
    private String description = new String();


    @Ignore
    public Casa(String name, String description) {
        this.name = name;
        this.description = description;
    }


    public Casa(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    @Ignore
    Casa(Intent intent) {
        id = intent.getStringExtra(Casa.ID);
        name = intent.getStringExtra(Casa.NAME);
        description = intent.getStringExtra(Casa.DESCRIPTION);
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public static void packageIntent (Intent intent, String name, String description) {
        intent.putExtra(Casa.NAME, name);
        intent.putExtra(Casa.DESCRIPTION, description);
    }

    public String toString() {
        return  id + ITEM_SEP + name + ITEM_SEP + description;
    }

    public String toLog() {
        return "ID: " + id + ITEM_SEP + " Nombre: " + name + ITEM_SEP +
                " Descripción: " + description + ITEM_SEP;
    }
}
