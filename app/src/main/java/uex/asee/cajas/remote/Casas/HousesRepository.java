package uex.asee.cajas.remote.Casas;

import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;

import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.Roomdb.CasaDAO;
import uex.asee.cajas.remote.AppExecutors;

public class HousesRepository {
    private static final String LOG_TAG = HousesRepository.class.getSimpleName();

    // For Singleton instantiation
    private static HousesRepository sInstance;
    private static AppExecutors appExecutors = AppExecutors.getInstance();
    private final CasaDAO mCasaDAO;
    private final HousesNetworkDataSource mHousesNetworkDataSource;

    private HousesRepository(CasaDAO casaDAO, HousesNetworkDataSource housesNetworkDataSource) {
        mCasaDAO = casaDAO;
        mHousesNetworkDataSource = housesNetworkDataSource;
        // LiveData that fetches repos from network
        mHousesNetworkDataSource.fetchHouses();
        LiveData<List<Casa>> networkData = mHousesNetworkDataSource.getCurrentHouses();

        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkData.observeForever(newCasasFromNetwork -> {
            appExecutors.diskIO().execute(() -> {
                // Deleting cached repos of user
                if (newCasasFromNetwork == null || newCasasFromNetwork.size() == 0){
                    mHousesNetworkDataSource.setFromDatabase(casaDAO.getAll().getValue());

                }
                else {
                    mCasaDAO.bulkInsert(newCasasFromNetwork);
                    Log.d(LOG_TAG, "New values inserted in Room");
                }

            });
        });
    }

    public synchronized static HousesRepository getInstance(CasaDAO dao, HousesNetworkDataSource nds) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new HousesRepository(dao, nds);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }


    /**
     * Database related operations
     **/

    public LiveData<List<Casa>> getCurrentHouses() {
        //  Return LiveData from Room. Use Transformation to get owner
        return mHousesNetworkDataSource.getCurrentHouses();
    }



}
