package uex.asee.cajas.Interfaces;

import uex.asee.cajas.Models.Box;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface BoxAPI {
    @GET("caja")
    public Call<List<Box>> getBoxes(@Header("auth-token") String authtoken);

    @GET("caja/id")
    public Call<Box> getBox(@Header("auth-token") String authtoken,@Query("id") String id);


    @Multipart
    @POST("caja")
    Call<Box> createBox(@Header("auth-token") String authtoken,
                        @Part("name") RequestBody name,
                        @Part("idcasa") RequestBody  idcasa,
                        @Part MultipartBody.Part image,
                        @Part("description") RequestBody  description);
    @DELETE("caja")
    Call <ResponseBody> deleteBox(@Header("auth-token")String authtoken, @Query("id") String id);

    @Multipart
    @PUT("caja")
    Call<Box> updateBox(@Header("auth-token") String authtoken,@Query("id") String id,
                        @Part("name") RequestBody name,
                        @Part("idcasa") RequestBody  idcasa,
                        @Part MultipartBody.Part image,
                        @Part("description") RequestBody  description);
}
