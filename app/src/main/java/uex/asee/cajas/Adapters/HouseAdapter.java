package uex.asee.cajas.Adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import uex.asee.cajas.Entidades.Casa;

import uex.asee.cajas.R;

import java.util.List;

public class HouseAdapter extends RecyclerView.Adapter <HouseAdapter.ViewHolder> {
    private Context context;
    private List<Casa> casas;
    private OnClickListener ocl;
    LayoutInflater inflater;

    public HouseAdapter(Context c, List<Casa> items, OnClickListener ol) {
        this.context = c;
        this.casas = items;
        this.ocl = ol;
    }
    public interface OnClickListener{
        void OnClick(Casa casa);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reclycler_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(casas,position,ocl);
    }


    @Override
    public int getItemCount() {
        if(casas == null){
            return 0;
        }
        return casas.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView nombreItem;

        public ViewHolder(View itemView){
            super(itemView);
            nombreItem = itemView.findViewById(R.id.nombre_casa);
        }

        public void bind (final List<Casa> cas, int position, final OnClickListener ol){
            nombreItem.setText(cas.get(position).getName());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ol.OnClick(cas.get(position));
                }
            });
        }

    }

    public void swap (List<Casa> lista_casa){
        casas = lista_casa;
    }

}
