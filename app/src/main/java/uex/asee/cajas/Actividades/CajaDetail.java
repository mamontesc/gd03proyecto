package uex.asee.cajas.Actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import uex.asee.cajas.AppContainer;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.Entidades.Objeto;
import uex.asee.cajas.Interfaces.BoxAPI;
import uex.asee.cajas.Interfaces.ImageAPI;
import uex.asee.cajas.Adapters.ItemAdapter;
import uex.asee.cajas.Models.Box;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uex.asee.cajas.ui.HousesViewModel;
import uex.asee.cajas.ui.ItemsViewModel;

public class CajaDetail extends AppCompatActivity {
    public static final String CAJA_ID = "CAJA_ID";
    private String caja_id;
    private Caja cajaDetallada;
    private TextView name_caja;
    private TextView descCaja;
    private TextView casaCaja;
    private List<Objeto> objetos;
    GridView gridView;
    ImageView imageView;
    ItemsViewModel mViewModel;
    ItemAdapter itemAdapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caja_detail);
        caja_id = getIntent().getStringExtra(CAJA_ID);
        name_caja = (TextView) findViewById(R.id.name_caja);
        descCaja = (TextView) findViewById(R.id.desc_caja);
        casaCaja = (TextView) findViewById(R.id.lacasa_caja);
        imageView = findViewById(R.id.image_detail_box);
        gridView = findViewById(R.id.grid_caja_detail);
    }

    @Override
    protected void onStart(){
        super.onStart();
        cargarDatos();
    }

    private void showDetails(){
        name_caja.setText(cajaDetallada.getName());
        descCaja.setText(cajaDetallada.getDescription());
        ImageAPI imageAPI = RetrofitClient.getClient().create(ImageAPI.class);
        Call<String> call = imageAPI.downloadFileWithDynamicUrlSync(cajaDetallada.getImagen());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    byte [] decodedString = Base64.getDecoder().decode(response.body());
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);
                    imageView.setImageBitmap(decodedByte);
                } else {
                    Picasso.get().load("https://res.cloudinary.com/teepublic/image/private/s--1GXM2y6u--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_auto,h_630,q_90,w_630/v1587938872/production/designs/9519223_0.jpg").into(imageView);
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println(t.getCause().toString());
                Picasso.get().load("https://res.cloudinary.com/teepublic/image/private/s--1GXM2y6u--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_auto,h_630,q_90,w_630/v1587938872/production/designs/9519223_0.jpg").into(imageView);
            }
        });
            AppContainer appContainer = MainActivity.appContainer;
            HousesViewModel mViewModelH = new ViewModelProvider(this, appContainer.housesfactory).get(HousesViewModel.class);

            mViewModelH.getCasas().observe(this, casas -> {

            if (cajaDetallada.getIdCasa() != null) {
                if(casas != null) {
                    for (Casa house : casas) {
                        if (house.getId().equals(cajaDetallada.getIdCasa())) {
                            casaCaja.setText(house.getName());
                        }
                    }
                }
            }
             });
            itemAdapter = new ItemAdapter(this, new ArrayList<>(), (objeto,view) -> {
                Intent intent = new Intent(this, ItemDetail.class);
                intent.putExtra(ItemDetail.ITEM_ID, objeto.getId());
                startActivity(intent);
            });



            mViewModel = new ViewModelProvider(this, appContainer.itemsfactory).get(ItemsViewModel.class);

            mViewModel.getItemsById(cajaDetallada.getId()).observe(this, items -> {
                itemAdapter.swap(items);
                gridView.setAdapter(itemAdapter);
            });


            Button editar = (Button) findViewById(R.id.button_edit_box);
            editar.setOnClickListener(view -> {
                Intent intent = new Intent(getApplicationContext(), EditCaja.class);
                intent.putExtra(EditCaja.EDIT_CAJA_ID, caja_id);
                startActivity(intent);
            });
    }

    private void cargarDatos(){
        BoxAPI boxAPI = RetrofitClient.getClient().create(BoxAPI.class);
        Call<Box> call = boxAPI.getBox(Login.TOKEN.getToken(),caja_id);

        call.enqueue(new Callback<Box>() {
            @Override
            public void onResponse(Call<Box> call, Response<Box> response) {
                if(response.isSuccessful()){
                    Box caja = response.body();
                    cajaDetallada = new Caja(caja.get_id(),caja.getName(),caja.getIdcasa(), caja.getDescription(), caja.getUserid(),caja.getImginfo());
                    runOnUiThread(() -> showDetails());
                }else{
                    AppExecutors.getInstance().diskIO().execute(() -> {
                        InventarioDatabase database = InventarioDatabase.getInstance(CajaDetail.this);
                        cajaDetallada = database.getDaoCaja().getCajaById(caja_id);
                        runOnUiThread(() -> showDetails());
                    });
                }
            }
            @Override
            public void onFailure(Call<Box> call, Throwable t) {
                AppExecutors.getInstance().diskIO().execute(() -> {
                    InventarioDatabase database = InventarioDatabase.getInstance(CajaDetail.this);
                    cajaDetallada = database.getDaoCaja().getCajaById(caja_id);
                    runOnUiThread(() -> showDetails());
                });
            }
        });
    }
}