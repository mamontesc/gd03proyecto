package uex.asee.cajas.Actividades;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import uex.asee.cajas.AppContainer;
import uex.asee.cajas.Interfaces.BoxAPI;
import uex.asee.cajas.Interfaces.ImageAPI;
import uex.asee.cajas.Models.Box;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Entidades.Objeto;
import uex.asee.cajas.Interfaces.ItemAPI;
import uex.asee.cajas.Models.Item;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uex.asee.cajas.ui.BoxesViewModel;

public class EditItem extends AppCompatActivity {

    public static final String EDIT_ITEM_ID = "EDIT_ITEM_ID";
    private Objeto itemDetallado;
    ArrayList<String> cajasNames= new ArrayList<String>();
    ArrayList<Caja> boxes= new ArrayList<>();
    Spinner spinner;
    Spinner cajaSpinner;
    EditText nItemName;
    EditText nItemDescription;
    String item_id;
    int pos = 0;
    ImageView imageView;
    Uri auxuri;
    String idCaja;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);
        item_id = getIntent().getStringExtra(EDIT_ITEM_ID);
        nItemName = (EditText) findViewById(R.id.eitem_name);
        nItemDescription = (EditText) findViewById(R.id.eitem_description);
        spinner = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(this, R.array.categorias, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinner.setAdapter(adapter);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        imageView = findViewById(R.id.image_selected_edit_item);
        ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
                uri -> {
                    System.out.println(uri);
                    imageView.setImageURI(uri);
                    auxuri = uri;
                });
        Button btnChooseFile = findViewById(R.id.select_photo_item_edit);
        btnChooseFile.setOnClickListener(view -> {
            mGetContent.launch("image/*");
        });
        cajaSpinner = findViewById(R.id.spinnerbox);
        cargarDatos();
        AppContainer appContainer = MainActivity.appContainer;

        BoxesViewModel mViewModel = new ViewModelProvider(this, appContainer.boxesfactory).get(BoxesViewModel.class);

        mViewModel.getBoxes().observe(this, cajas -> {

            if (cajas != null) {
                boxes = (ArrayList<Caja>) cajas;
                System.out.println("TAMAÑO CASAS " + boxes.size());
                for (Caja box : boxes) {
                    cajasNames.add(box.getName());

                }
                System.out.println("TAMAÑO CASASNOM " + cajasNames.size());
            }
            ArrayAdapter<String> adapter2=new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, cajasNames);
            cajaSpinner.setAdapter(adapter2);
            cajaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (spinner.getSelectedItemPosition() > -1){
                        idCaja = boxes.get(spinner.getSelectedItemPosition()).getId();
                        System.out.println("HOLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        });

    }
    private void cargarDatos(){
        ItemAPI itemAPI = RetrofitClient.getClient().create(ItemAPI.class);
        Call<Item> call = itemAPI.getItem(Login.TOKEN.getToken(),item_id);

        call.enqueue(new Callback<Item>() {
            @Override
            public void onResponse(Call<Item> call, Response<Item> response) {
                if(response.isSuccessful()){
                    Item item = response.body();
                    itemDetallado = new Objeto(item.get_id(),item.getName(),item.getIdcaja(), item.getDescription(), item.getUserid(),item.getImginfo());
                    runOnUiThread(() -> showDetails());
                }else{
                    AppExecutors.getInstance().diskIO().execute(() -> {
                        InventarioDatabase database = InventarioDatabase.getInstance(EditItem.this);
                        itemDetallado = database.getDaoObjeto().getObjetoById(item_id);
                        runOnUiThread(() -> showDetails());
                    });
                }
            }
            @Override
            public void onFailure(Call<Item> call, Throwable t) {
                AppExecutors.getInstance().diskIO().execute(() -> {
                    InventarioDatabase database = InventarioDatabase.getInstance(EditItem.this);
                    itemDetallado = database.getDaoObjeto().getObjetoById(item_id);
                    runOnUiThread(() -> showDetails());
                });
            }
        });
    }
    private void showDetails(){
        nItemName.setText(itemDetallado.getName());
        nItemDescription.setText(itemDetallado.getDescription());
        ImageAPI imageAPI = RetrofitClient.getClient().create(ImageAPI.class);
        Call<String> call = imageAPI.downloadFileWithDynamicUrlSync(itemDetallado.getImagen());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    byte [] decodedString = Base64.getDecoder().decode(response.body());
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);
                    imageView.setImageBitmap(decodedByte);
                } else {
                    Picasso.get().load("https://res.cloudinary.com/teepublic/image/private/s--1GXM2y6u--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_auto,h_630,q_90,w_630/v1587938872/production/designs/9519223_0.jpg").into(imageView);
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println(t.getCause().toString());
                Picasso.get().load("https://res.cloudinary.com/teepublic/image/private/s--1GXM2y6u--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_auto,h_630,q_90,w_630/v1587938872/production/designs/9519223_0.jpg").into(imageView);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_cancel:
                this.finish();
                return true;
            case R.id.action_confirm:
                itemDetallado.setName(nItemName.getText().toString());
                itemDetallado.setDescription(nItemDescription.getText().toString());
                itemDetallado.setCategoria(spinner.getSelectedItem().toString());
                itemDetallado.setCaja(idCaja);
                if(!nItemName.getText().toString().isEmpty() && !nItemDescription.getText().toString().isEmpty()){
                    updateItem(itemDetallado.getName(),itemDetallado.getDescription(),itemDetallado.getCaja(),itemDetallado.getCategoria());
                    this.finish();
                    return true;
                }else{
                    Toast.makeText(EditItem.this, "Por favor rellena todos los campos", Toast.LENGTH_SHORT).show();
                }

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void updateItem(String name,String description,String idcaja,String categoria){
        ItemAPI itemAPI = RetrofitClient.getClient().create(ItemAPI.class);
        RequestBody rname = RequestBody.create(MediaType.parse("text/plain"),name);
        RequestBody rdescription = RequestBody.create(MediaType.parse("text/plain"),description);
        RequestBody ridcaja = null;
        if(idcaja!= null){
            ridcaja = RequestBody.create(MediaType.parse("text/plain"),idcaja);
        }
        RequestBody rcategoria = RequestBody.create(MediaType.parse("text/plain"),categoria);
        RequestBody rimg = null;
        MultipartBody.Part rbimg = null;
        try {
            if(auxuri != null){
                InputStream is = getContentResolver().openInputStream(auxuri);
                if(is!= null){
                    rimg = RequestBody.create(
                            MediaType.parse(getContentResolver().getType(auxuri)),
                            getBytes(is)

                    );
                    MimeTypeMap mime = MimeTypeMap.getSingleton();
                    String type = mime.getExtensionFromMimeType(getContentResolver().getType(auxuri));

                    rbimg =  MultipartBody.Part.createFormData("image", name+"."+type, rimg);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Call<Item> call = itemAPI.updateItem(Login.TOKEN.getToken(),item_id,rname,rdescription,rbimg,ridcaja,rcategoria);

        call.enqueue(new Callback<Item>() {
            @Override
            public void onResponse(Call<Item> call, Response<Item> response) {
                if(response.isSuccessful()){
                    String id = response.body().get_id();
                    String name =  response.body().getName();
                    String idCaja =  response.body().getIdcaja();
                    String description = response.body().getDescription();
                    String userId = response.body().getUserid();
                    String imagen = response.body().getImginfo();
                    String categoria = response.body().getCategoria();
                    if(imagen == null){
                        imagen = "";
                    }
                    if(idCaja == null){
                        idCaja = "";
                    }
                    if(description == null){
                        description = "";
                    }

                    Objeto item = new Objeto(id,name,categoria,imagen,description,idCaja,userId);
                    if(item != null){
                        AppExecutors.getInstance().diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                InventarioDatabase database = InventarioDatabase.getInstance(EditItem.this);
                                database.getDaoObjeto().update(item);
                            }
                        });
                    }
                }else{
                    Toast.makeText(EditItem.this, "Error al crear caja", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Item> call, Throwable t) {
                System.out.println(t.getCause().toString());
                Toast.makeText(EditItem.this, "Error al conectarse con los servicios", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public byte[] getBytes(InputStream is) throws IOException {
        ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();

        int buffSize = 1024;
        byte[] buff = new byte[buffSize];

        int len = 0;
        while ((len = is.read(buff)) != -1) {
            byteBuff.write(buff, 0, len);
        }

        return byteBuff.toByteArray();
    }
}