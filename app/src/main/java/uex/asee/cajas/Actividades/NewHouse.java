package uex.asee.cajas.Actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.Interfaces.HouseAPI;
import uex.asee.cajas.Models.House;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewHouse extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_house);
        final EditText nItemName = (EditText) findViewById(R.id.nhouse_name);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final EditText nHouseName = (EditText) findViewById(R.id.nhouse_name);
        final EditText nHouseDesc = (EditText) findViewById(R.id.nhouse_desc);
        switch (item.getItemId()) {
            case R.id.action_cancel:
                this.finish();
                return true;
            case R.id.action_confirm:
                createHouse(nHouseName.getText().toString(),nHouseDesc.getText().toString());
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void createHouse(String name,String description){
        HouseAPI houseAPI = RetrofitClient.getClient().create(HouseAPI.class);
        Call<House> call = houseAPI.createHouse(Login.TOKEN.getToken(),name,description);

        call.enqueue(new Callback<House>() {
            @Override
            public void onResponse(Call<House> call, Response<House> response) {
                if(response.isSuccessful()){
                    Casa house = new Casa(response.body().get_id(),response.body().getName(),response.body().getDescription());
                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            InventarioDatabase database = InventarioDatabase.getInstance(NewHouse.this);
                            database.getDaoCasa().insert(house);
                        }
                    });
                }else{
                    Toast.makeText(NewHouse.this, "Error al conectarse con los servicios", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<House> call, Throwable t) {
                Toast.makeText(NewHouse.this, "Error al conectarse con los servicios", Toast.LENGTH_SHORT).show();
            }
        });

    }



}