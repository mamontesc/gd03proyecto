package uex.asee.cajas.Actividades;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import uex.asee.cajas.AppContainer;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.Interfaces.HouseAPI;
import uex.asee.cajas.Adapters.ItemAdapterCaja;
import uex.asee.cajas.Models.House;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uex.asee.cajas.ui.BoxesViewModel;

public class CasaDetail extends AppCompatActivity {
    public static final String HOUSE_ID = "HOUSE_ID";
    private String house_id;
    private Casa casaDetallada;
    private TextView textExtendida;
    private TextView descHouse;
    ItemAdapterCaja itemAdapterCaja ;
    public static List<Casa> casas = new ArrayList<Casa>();
    List<Caja> boxes = new ArrayList<>();
    GridView gridView;
    BoxesViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_casa_detail);
        house_id = getIntent().getStringExtra(HOUSE_ID);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setContentView(R.layout.activity_casa_detail);
        cargarDatos();
    }

    private void showDetails(){
        textExtendida = findViewById(R.id.item_name);
        descHouse =  findViewById(R.id.house_desc);
        gridView = findViewById(R.id.grid);
        textExtendida.setText(casaDetallada.getName());
        descHouse.setText(casaDetallada.getDescription());
        Button editar = (Button) findViewById(R.id.button_edit_box);
        editar.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), EditCasa.class);
            intent.putExtra(EditCasa.HOUSE_EDIT_ID, house_id);
            startActivity(intent);
        });
        Button eliminar = (Button) findViewById(R.id.deletecasa);
        eliminar.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("¿Estas seguro de que deseas eliminar la casa?")
                    .setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deleteHouse();
                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        });
        AppContainer appContainer = MainActivity.appContainer;

        itemAdapterCaja = new ItemAdapterCaja(this, new ArrayList<>(), (caja,view) -> {
            Intent intent = new Intent(this , CajaDetail.class);
            intent.putExtra(CajaDetail.CAJA_ID, caja.getId());
            startActivity(intent);
        });



        mViewModel = new ViewModelProvider(this, appContainer.boxesfactory).get(BoxesViewModel.class);
        System.out.println(casaDetallada.getId());
        mViewModel.getCasasById(casaDetallada.getId()).observe(this, boxes -> {
            itemAdapterCaja.swap(boxes);
            gridView.setAdapter(itemAdapterCaja);
        });





    }

    private void deleteHouse(){
        HouseAPI houseAPI = RetrofitClient.getClient().create(HouseAPI.class);
        Call<ResponseBody> call = houseAPI.deleteHouse(Login.TOKEN.getToken(),house_id);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    AppExecutors.getInstance().diskIO().execute(() -> {
                        InventarioDatabase database = InventarioDatabase.getInstance(CasaDetail.this);
                        database.getDaoCasa().deleteCasaById(house_id);
                    });
                }else{
                    Toast.makeText(CasaDetail.this, "Ha ocurrido un error al borrar la casa", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(CasaDetail.this, "Error al conectarse con los servicios", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void cargarDatos(){
        HouseAPI houseAPI = RetrofitClient.getClient().create(HouseAPI.class);
        System.out.println(house_id);
        Call<House> call = houseAPI.getHouse(Login.TOKEN.getToken(),house_id);

        call.enqueue(new Callback<House>() {
            @Override
            public void onResponse(Call<House> call, Response<House> response) {
                if(response.isSuccessful()){
                    House casa = response.body();
                    casaDetallada = new Casa(casa.get_id(),casa.getName(),casa.getDescription());
                    AppExecutors.getInstance().diskIO().execute(() -> {
                        InventarioDatabase database = InventarioDatabase.getInstance(CasaDetail.this);
                        boxes = database.getDaoCaja().getAllByCasa(casaDetallada.getId());
                        runOnUiThread(() -> showDetails());
                    });
                }else{
                    AppExecutors.getInstance().diskIO().execute(() -> {
                        InventarioDatabase database = InventarioDatabase.getInstance(CasaDetail.this);
                        casaDetallada = database.getDaoCasa().getCasaById(house_id);
                        boxes = database.getDaoCaja().getAllByCasa(casaDetallada.getId());
                        runOnUiThread(() -> showDetails());
                    });
                }
            }

            @Override
            public void onFailure(Call<House> call, Throwable t) {
                AppExecutors.getInstance().diskIO().execute(() -> {
                    InventarioDatabase database = InventarioDatabase.getInstance(CasaDetail.this);
                    casaDetallada = database.getDaoCasa().getCasaById(house_id);
                    boxes = database.getDaoCaja().getAllByCasa(casaDetallada.getId());
                    runOnUiThread(() -> showDetails());
                });
            }
        });
    }
}