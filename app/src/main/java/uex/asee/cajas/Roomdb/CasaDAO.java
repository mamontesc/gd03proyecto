package uex.asee.cajas.Roomdb;

import static androidx.room.OnConflictStrategy.REPLACE;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import uex.asee.cajas.Entidades.Casa;

import java.util.List;

@Dao
public interface CasaDAO {
    @Query("SELECT * FROM casa")
    public LiveData<List<Casa>> getAll();

    @Insert
    public void insert(Casa casa);

    @Query("DELETE FROM casa")
    public void deleteAll();

    @Query("DELETE FROM casa WHERE ID = :id")
    public void deleteCasaById(String id);

    @Insert(onConflict = REPLACE)
    public  void bulkInsert(List<Casa> casas);

    @Update
    public int update(Casa casa);

    @Query("SELECT * FROM casa WHERE ID = :id")
    public Casa getCasaById(String id);
}
