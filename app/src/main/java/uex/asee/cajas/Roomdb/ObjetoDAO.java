package uex.asee.cajas.Roomdb;

import static androidx.room.OnConflictStrategy.REPLACE;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import uex.asee.cajas.Entidades.Objeto;

import java.util.List;

@Dao
public interface ObjetoDAO {
    @Query("SELECT * FROM objeto")
    public LiveData<List<Objeto>> getAll();

    @Insert
    public void insert(Objeto obj);

    @Query("DELETE FROM objeto")
    public void deleteAll();

    @Query("DELETE FROM objeto WHERE ID = :id")
    public void deleteObjetoById(String id);

    @Insert(onConflict = REPLACE)
    public void bulkInsert(List<Objeto> objetos);
    @Update
    public void update(Objeto obj);

    @Query("SELECT * FROM objeto WHERE ID = :id")
    public Objeto getObjetoById(String id);

    @Query("SELECT * FROM objeto WHERE caja = :id")
    public List<Objeto> getAllByCaja(String id);
}
