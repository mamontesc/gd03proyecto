package uex.asee.cajas.Fragmentos;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import uex.asee.cajas.Actividades.Login;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Interfaces.UserAPI;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SettingsFragment extends PreferenceFragmentCompat {

    public static final String KEY_PREF_NIGHTMODE = "pref_nightmode";
    public static final String KEY_PREF_RESET = "pref_reset";


    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

        addPreferencesFromResource(R.xml.preferences);


        Preference mode = findPreference(KEY_PREF_NIGHTMODE);

        mode.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(preference.getKey(), false)){


                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
                else{
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
                return false;
            }
        });


        Preference reset = findPreference(KEY_PREF_RESET);

        reset.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("¿Estás seguro de que quieres borrar todos tus datos?")
                        .setCancelable(false)
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                System.out.println("Ha aceptado");
                                UserAPI userAPI = RetrofitClient.getClient().create(UserAPI.class);
                                Call<ResponseBody> call = userAPI.deleteAll(Login.TOKEN.getToken());
                                call.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        if(response.isSuccessful()){
                                            System.out.println("Bieeeen");
                                            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                                                @Override
                                                public void run() {
                                                    InventarioDatabase database = InventarioDatabase.getInstance(getContext());
                                                    database.getDaoCaja().deleteAll();
                                                    database.getDaoObjeto().deleteAll();
                                                    database.getDaoCasa().deleteAll();
                                                }
                                            });
                                        }else{
                                            getActivity().runOnUiThread(()-> Toast.makeText(getContext(), "Error al borrar los datos del usuario", Toast.LENGTH_SHORT).show());
                                        }
                                    }
                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                        getActivity().runOnUiThread(()-> Toast.makeText(getContext(), "Error al borrar los datos del usuario", Toast.LENGTH_SHORT).show());
                                    }
                                });
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return false;
            }
        });

    }



}
