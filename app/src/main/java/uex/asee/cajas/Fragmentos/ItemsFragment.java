package uex.asee.cajas.Fragmentos;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import uex.asee.cajas.Actividades.CasaDetail;
import uex.asee.cajas.Actividades.Houses;
import uex.asee.cajas.Actividades.ItemDetail;
import uex.asee.cajas.Actividades.Login;
import uex.asee.cajas.Actividades.MainActivity;
import uex.asee.cajas.Actividades.NewItem;
import uex.asee.cajas.Actividades.deleteItem;
import uex.asee.cajas.Adapters.HouseAdapter;
import uex.asee.cajas.AppContainer;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Objeto;
import uex.asee.cajas.Interfaces.ItemAPI;
import uex.asee.cajas.Adapters.ItemAdapter;
import uex.asee.cajas.Models.Item;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uex.asee.cajas.ui.HousesViewModel;
import uex.asee.cajas.ui.ItemsViewModel;

public class ItemsFragment extends Fragment {
    private boolean isFABOpen = false;
    GridView gridView;
    SwipeRefreshLayout swipeLayout;
    ItemAdapter itemAdapter ;
    ItemsViewModel mViewModel;

    public ItemsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_items, container, false);
        gridView = view.findViewById(R.id.grid);

        AppContainer appContainer = MainActivity.appContainer;

        itemAdapter = new ItemAdapter(getContext(), new ArrayList<>(), (objeto,view1) -> {
            Intent intent = new Intent(getActivity(), ItemDetail.class);
            intent.putExtra(ItemDetail.ITEM_ID, objeto.getId());
            startActivity(intent);
        });

        mViewModel = new ViewModelProvider(this, appContainer.itemsfactory).get(ItemsViewModel.class);

        mViewModel.getItems().observe(getViewLifecycleOwner(), objetos -> {
            itemAdapter.swap(objetos);
        });


        gridView.setAdapter(itemAdapter);
        swipeLayout = view.findViewById(R.id.swiperefreshitem);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        swipeLayout.post(()->{
            swipeLayout.setRefreshing(true);
            mViewModel.getItems().observe(getViewLifecycleOwner(), objetos -> {
                itemAdapter.swap(objetos);
                swipeLayout.setRefreshing(false);
            });
        });

        swipeLayout.setOnRefreshListener(() -> {
            swipeLayout.setRefreshing(true);
            mViewModel.onRefresh();
            mViewModel.getItems().observe(getViewLifecycleOwner(), objetos -> {
                itemAdapter.swap(objetos);
                swipeLayout.setRefreshing(false);
            });

        });

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FloatingActionButton fab = view.findViewById(R.id.fab);
        FloatingActionButton fab1 = view.findViewById(R.id.fab1);
        FloatingActionButton fab2 = view.findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFABOpen){
                    showFABMenu(fab1,fab2);
                }else{
                    closeFABMenu(fab1,fab2);
                }
            }
        });
        fab1.setOnClickListener(view1 -> {
            Intent intent = new Intent(getActivity(), NewItem.class);
            startActivity(intent);
        });
        fab2.setOnClickListener(view12 -> {
            Intent intent = new Intent(getActivity(), deleteItem.class);
            startActivity(intent);
        });

    }
    private void showFABMenu(FloatingActionButton fab1,FloatingActionButton fab2 ){
        isFABOpen=true;
        fab1.animate().translationY(-getResources().getDimension(R.dimen.standard_60));
        fab2.animate().translationY(-getResources().getDimension(R.dimen.standard_120));
    }

    private void closeFABMenu(FloatingActionButton fab1,FloatingActionButton fab2){
        isFABOpen=false;
        fab1.animate().translationY(0);
        fab2.animate().translationY(0);
    }

}
