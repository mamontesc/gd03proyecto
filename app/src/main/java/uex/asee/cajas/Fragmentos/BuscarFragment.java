package uex.asee.cajas.Fragmentos;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import uex.asee.cajas.Actividades.CajaDetail;
import uex.asee.cajas.Actividades.ItemDetail;
import uex.asee.cajas.Actividades.MainActivity;
import uex.asee.cajas.Adapters.ItemAdapter;
import uex.asee.cajas.Adapters.ItemAdapterCaja;
import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Entidades.Objeto;
import uex.asee.cajas.R;
import uex.asee.cajas.ui.BoxesViewModel;
import uex.asee.cajas.ui.ItemsViewModel;

public class BuscarFragment extends Fragment {
    public BuscarFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buscar, container, false);

        EditText et = view.findViewById(R.id.TextoBuscar);
        Button bt = view.findViewById(R.id.BotonBuscar);

        TextView item_text = view.findViewById(R.id.TituloObjeto);
        TextView box_text = view.findViewById(R.id.TituloCaja);

        GridView items_list = view.findViewById(R.id.ObjetosGrid);
        GridView boxes_list = view.findViewById(R.id.CajasGrid);

        BoxesViewModel mBoxesViewModel = new ViewModelProvider(this, MainActivity.appContainer.boxesfactory).get(BoxesViewModel.class);
        ItemsViewModel mItemsViewModel = new ViewModelProvider(this, MainActivity.appContainer.itemsfactory).get(ItemsViewModel.class);

        bt.setOnClickListener(button_view -> {
            mBoxesViewModel.getBoxes().observe(getViewLifecycleOwner(), b -> {
                List<Caja> boxes = new ArrayList<>(b);
                boxes.removeIf(caja -> !caja.getName().contains(et.getText().toString()));

                boxes_list.setAdapter(new ItemAdapterCaja(getContext(), boxes, (box, v) -> {
                    Intent intent = new Intent(getActivity(), CajaDetail.class);
                    intent.putExtra(CajaDetail.CAJA_ID, box.getId());
                    startActivity(intent);
                }));

                if (boxes.size() > 0) box_text.setText("Cajas"); else box_text.setText("");
                if (boxes.size() == 0)
                    Toast.makeText(getContext(), "No hay resultados de cajas en tu búsqueda", Toast.LENGTH_SHORT).show();
            });

            mItemsViewModel.getItems().observe(getViewLifecycleOwner(), i -> {
                List<Objeto> items = new ArrayList<>(i);
                items.removeIf(objeto -> !objeto.getName().contains(et.getText().toString()));

                items_list.setAdapter(new ItemAdapter(getContext(), items, (item, v) -> {
                    Intent intent = new Intent(getActivity(), ItemDetail.class);
                    intent.putExtra(ItemDetail.ITEM_ID, item.getId());
                    startActivity(intent);
                }));

                if (items.size() > 0) item_text.setText("Objetos"); else item_text.setText("");
                if (items.size() == 0)
                    Toast.makeText(getContext(), "No hay resultados de objetos en tu búsqueda", Toast.LENGTH_SHORT).show();
            });
        });

        return view;
    }
}
